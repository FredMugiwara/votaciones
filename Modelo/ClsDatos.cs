﻿using System;

//Agregar 
using System.Data;
using MySql.Data.MySqlClient;

    public class clsDatos
    {
        #region Declaracion de Variables

        MySqlConnection cnnConexion = null;
        MySqlCommand cmdComando = null;
        MySqlDataAdapter daAdaptador = null;
        DataTable Dtt = null;
        IDataReader dr;
        string strCadenaConexion = string.Empty;

        #endregion

        #region Constructor

        public clsDatos()
        {
            strCadenaConexion = "Data Source=localhost;database=bdvotacion2;Uid=root";

        }



        #endregion

        #region Metodos a Ejecutar


        public int EjecutarSP(MySqlParameter[] parParametros, string parSPName)
        {

         
            int valor = 0;
            //try
            //{
               
                cnnConexion = new MySqlConnection(strCadenaConexion);
               
                cmdComando = new MySqlCommand();
                cmdComando.Connection = cnnConexion;
          
                cnnConexion.Open();
         
                cmdComando.CommandType = CommandType.StoredProcedure;
              
                cmdComando.CommandText = parSPName;
       
             

         
                cmdComando.Parameters.AddRange(parParametros);

        //Ejecutamos el TSQL(Transaction SQL) en el servidor.
                valor = cmdComando.ExecuteNonQuery();


        //}
        //catch (Exception ex)
        ////{
        ////    throw new Exception(ex.Message);
        //    return 3;
        //}

        //finally
        //{
        //    valor = cmdComando.ExecuteNonQuery();
        //    cnnConexion.Dispose();
        //    cmdComando.Dispose();

        //}
        return valor;
        }



    




        public DataTable RetornaTabla(MySqlParameter[] parParametros, string parTSQL)
        {
            Dtt = null;
            try
            {
                Dtt = new DataTable();
               
                cnnConexion = new MySqlConnection(strCadenaConexion);
               
                cmdComando = new MySqlCommand();
                cmdComando.Connection = cnnConexion;
            
                cmdComando.CommandType = CommandType.StoredProcedure;
                //Agregamos el nombre del Srore procedure.
                cmdComando.CommandText = parTSQL;
                //Agregmos los parametros a ejecutar
                cmdComando.Parameters.AddRange(parParametros);
                //Instanciamos el objeto Adaptador con el comando a utilizar
                daAdaptador = new MySqlDataAdapter(cmdComando);
                //Llenamos el Dataset con el adaptador de datos.
                daAdaptador.Fill(Dtt);
        }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
    }
            finally
            {
                cnnConexion.Dispose();
                cmdComando.Dispose();
                daAdaptador.Dispose();
            }
            return Dtt;
        }


        #endregion


    }
