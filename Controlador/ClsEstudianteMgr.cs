﻿using System;

using System.Data;
using MySql.Data.MySqlClient;

namespace CONTROLADOR
{ 
  public class ClsEstudianteMgr
    {
       
        clsDatos cnGeneral = new clsDatos();
        ClsEstudiante objEstudiante = new ClsEstudiante();
        DataTable tblDatos = null;


        public ClsEstudianteMgr(ClsEstudiante parObjCita)
        {
            objEstudiante = new ClsEstudiante();
            objEstudiante = parObjCita;
        }
        //RETORA TABLA CON LOS CONTACTOS
        public DataTable Listar()
        {

            tblDatos = new DataTable();

            try
            {
                cnGeneral = new clsDatos();
                MySqlParameter[] parParameter = new MySqlParameter[11];

                parParameter[0] = new MySqlParameter();
                parParameter[0].ParameterName = "opc";
                parParameter[0].MySqlDbType = MySqlDbType.Int32;
                parParameter[0].Value = objEstudiante.opc;

                parParameter[1] = new MySqlParameter();
                parParameter[1].ParameterName = "ENControl";
                parParameter[1].MySqlDbType = MySqlDbType.Int32;
                parParameter[1].Size = 30;
                parParameter[1].Value = objEstudiante.ENControl;

                parParameter[2] = new MySqlParameter();
                parParameter[2].ParameterName = "ENip";
                parParameter[2].MySqlDbType = MySqlDbType.Int32;
                parParameter[2].Size = 30;
                parParameter[2].Value = objEstudiante.ENip;

                parParameter[3] = new MySqlParameter();
                parParameter[3].ParameterName = "ENNombre";
                parParameter[3].MySqlDbType = MySqlDbType.VarChar;
                parParameter[3].Size = 30;
                parParameter[3].Value = objEstudiante.ENNombre;

                tblDatos = cnGeneral.RetornaTabla(parParameter, "SPLogin");

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return tblDatos;
        }

        //GUARDA CONTACTO
      
}

       
      

    }
}
