﻿namespace PlanillasTec
{
    partial class Configuracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtaPlanilla = new System.Windows.Forms.DataGridView();
            this.btnAgregarPlanilla = new System.Windows.Forms.Button();
            this.btnEliminarPlanilla = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnEditarPlanilla = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dtaPlanilla)).BeginInit();
            this.SuspendLayout();
            // 
            // dtaPlanilla
            // 
            this.dtaPlanilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtaPlanilla.Location = new System.Drawing.Point(12, 42);
            this.dtaPlanilla.Name = "dtaPlanilla";
            this.dtaPlanilla.Size = new System.Drawing.Size(415, 393);
            this.dtaPlanilla.TabIndex = 0;
            // 
            // btnAgregarPlanilla
            // 
            this.btnAgregarPlanilla.Location = new System.Drawing.Point(519, 135);
            this.btnAgregarPlanilla.Name = "btnAgregarPlanilla";
            this.btnAgregarPlanilla.Size = new System.Drawing.Size(129, 36);
            this.btnAgregarPlanilla.TabIndex = 1;
            this.btnAgregarPlanilla.Text = "Agergar Planilla";
            this.btnAgregarPlanilla.UseVisualStyleBackColor = true;
            // 
            // btnEliminarPlanilla
            // 
            this.btnEliminarPlanilla.Location = new System.Drawing.Point(519, 199);
            this.btnEliminarPlanilla.Name = "btnEliminarPlanilla";
            this.btnEliminarPlanilla.Size = new System.Drawing.Size(129, 36);
            this.btnEliminarPlanilla.TabIndex = 2;
            this.btnEliminarPlanilla.Text = "Eliminar Planilla";
            this.btnEliminarPlanilla.UseVisualStyleBackColor = true;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(593, 399);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(129, 36);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnEditarPlanilla
            // 
            this.btnEditarPlanilla.Location = new System.Drawing.Point(433, 399);
            this.btnEditarPlanilla.Name = "btnEditarPlanilla";
            this.btnEditarPlanilla.Size = new System.Drawing.Size(129, 36);
            this.btnEditarPlanilla.TabIndex = 4;
            this.btnEditarPlanilla.Text = "Editar";
            this.btnEditarPlanilla.UseVisualStyleBackColor = true;
            // 
            // Configuracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(734, 447);
            this.Controls.Add(this.btnEditarPlanilla);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnEliminarPlanilla);
            this.Controls.Add(this.btnAgregarPlanilla);
            this.Controls.Add(this.dtaPlanilla);
            this.Name = "Configuracion";
            this.Text = "Configuracion";
            this.Load += new System.EventHandler(this.Configuracion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtaPlanilla)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dtaPlanilla;
        private System.Windows.Forms.Button btnAgregarPlanilla;
        private System.Windows.Forms.Button btnEliminarPlanilla;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnEditarPlanilla;
    }
}